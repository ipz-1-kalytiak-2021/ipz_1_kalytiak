﻿
namespace ipz
{
    partial class FormStart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TITLE = new System.Windows.Forms.Label();
            this.slogan = new System.Windows.Forms.Label();
            this.dialogue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TITLE
            // 
            this.TITLE.Font = new System.Drawing.Font("Nirmala UI", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TITLE.ForeColor = System.Drawing.Color.DeepPink;
            this.TITLE.Location = new System.Drawing.Point(103, 87);
            this.TITLE.Name = "TITLE";
            this.TITLE.Size = new System.Drawing.Size(580, 120);
            this.TITLE.TabIndex = 2;
            this.TITLE.Text = "eBOX\r\n";
            this.TITLE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // slogan
            // 
            this.slogan.Font = new System.Drawing.Font("Nirmala UI", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.slogan.ForeColor = System.Drawing.Color.White;
            this.slogan.Location = new System.Drawing.Point(158, 207);
            this.slogan.Name = "slogan";
            this.slogan.Size = new System.Drawing.Size(477, 43);
            this.slogan.TabIndex = 4;
            this.slogan.Text = "The easiest way to get your money";
            this.slogan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dialogue
            // 
            this.dialogue.AutoSize = true;
            this.dialogue.BackColor = System.Drawing.Color.Gray;
            this.dialogue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dialogue.ForeColor = System.Drawing.Color.Black;
            this.dialogue.Location = new System.Drawing.Point(290, 418);
            this.dialogue.Name = "dialogue";
            this.dialogue.Size = new System.Drawing.Size(191, 20);
            this.dialogue.TabIndex = 5;
            this.dialogue.Text = "Press any key to use ATM";
            // 
            // FormStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.dialogue);
            this.Controls.Add(this.slogan);
            this.Controls.Add(this.TITLE);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormStart";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Start";
            this.Load += new System.EventHandler(this.Start_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Start_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TITLE;
        private System.Windows.Forms.Label slogan;
        private System.Windows.Forms.Label dialogue;
    }
}